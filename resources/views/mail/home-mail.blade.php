@component('mail::message')
# Introduction

The body of your message.

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius dignissimos laboriosam, vitae sed molestias alias atque sapiente, cumque commodi dolor ipsa corporis magni necessitatibus quas molestiae ex nesciunt, perspiciatis ab.</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
