@extends('layouts.app')
@section('title', $article->title)
@section('content')
<div class="card">
	<div class="card-title">{{ $article->title }}</div>
	<div class="card-body">
		<table class="table table-bordered">
			<tbody>
					<tr>
					   	<th>Title</th>
   					   	<td>{{ $article->title }}</td>
					</tr>
					<tr>
					   	<th>Category</th>
   					   	<td>{{ optional($article->category)->name ?? 'Not Available' }}</td>
					</tr>
					<tr>
					   	<th>Excerpt</th>
   					   	<td>{{ $article->excerpt }}</td>
					</tr>
					<tr>
					   	<th>Content</th>
   					   	<td>{{ $article->content }}</td>
					</tr>
					<tr>
					   	<th>Created At</th>
					   	<td>{{ $article->created_at }}</td>
					</tr>
					<tr>
					   	<th>Updated At</th>
					   	<td>{{ $article->updated_at }}</td>
					</tr>
					<tr>
					   	<th>Image</th>
					   	<td>
					   		<img src="{{ Storage::url($article->image) }}" alt="image" class="img-fluid">
					   	</td>
					</tr>
				   </tr>
			</tbody>
		</table>
	</div>
</div>
@endsection