@extends('layouts.app')
@section('title', 'Create Article')
@section('content')
<div class="card">
	<div class="card-body">
		<form action="{{ route('articles.store') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="mb-3">
			  <label for="title" class="form-label">Title</label>
			  <input type="text" class="form-control @error('title') is-invalid @enderror" id="titles" name="title" value="{{ old('title') }}" >
			  @error('title')
			      <span class="invalid-feedback" role="alert">
			          <strong>{{ $message }}</strong>
			      </span>
			  @enderror
			</div>
			<div class="mb-3">
			  <label for="content" class="form-label">Category</label>
				<select name="category_id" id="" class="form-control">
					@foreach($categories as $category)
						<option value="{{$category->id}}">{{$category->name}}</option>
					@endforeach
				</select>
			  @error('content')
			      <span class="invalid-feedback" role="alert">
			          <strong>{{ $message }}</strong>
			      </span>
			  @enderror
			</div>
			<div class="mb-3">
			  <label for="excerpt" class="form-label">Excerpt</label>
			  <textarea class="form-control @error('excerpt') is-invalid @enderror" name="excerpt"  rows="3">{{old('excerpt')}}</textarea>
			  @error('excerpt')
			      <span class="invalid-feedback" role="alert">
			          <strong>{{ $message }}</strong>
			      </span>
			  @enderror
			</div>
			<div class="mb-3">
			  <label for="content" class="form-label">Content</label>
			  <textarea class="form-control @error('content') is-invalid @enderror"  id="content" name="content" rows="6">{{old('content')}}</textarea>
			  @error('content')
			      <span class="invalid-feedback" role="alert">
			          <strong>{{ $message }}</strong>
			      </span>
			  @enderror
			</div>
			<div class="mb-3">
				<input type="file" name="image" class="form-control @error('image') is-invalid @enderror">
				@error('image')
				    <span class="invalid-feedback" role="alert">
				        <strong>{{ $message }}</strong>
				    </span>
				@enderror
			</div>
			<div class="mb-3">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
		</form>
	</div>
</div>
@endsection