@extends('layouts.app')
@section('title', 'Articles')
@section('content')
<div class="card">
	<div class="card-title">Articles</div>
	<a href="{{ route('articles.create') }}" class="btn btn-primary">Create</a>
	<div class="card-body">
		@if(Session::has('success'))
			<div class="alert alert-success">{{Session::get('success')}}</div>
		@endif
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Title</th>
					<th>Excerpt</th>
					<th>Created At</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($articles as $key => $article)
				   <tr>
				   	<td>{{ ++$key }}</td>
				   	<td>{{ $article->title }}</td>
				   	<td>{{ $article->excerpt }}</td>
				   	<td>{{ $article->created_at }}</td>
				   	<td>
				   		<a href="{{ route('articles.show', $article->id) }}" class="btn btn-primary btn-sm">Show</a>
				   		<a href="{{ route('articles.edit', $article->id) }}" class="btn btn-warning btn-sm">Edit</a>
				   		<form action="{{ route('articles.delete', $article->id) }}" method="POST" onClick="return confirm('Are you sure?')">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
				   	</td>
				   </tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection