@extends('layouts.app')
@section('title', 'Edit Category')
@section('content')
<div class="card">
	<div class="card-body">
		<form action="{{ route('categories.update', $category->id) }}" method="POST">
			@csrf
			@method('PATCH')
			<div class="mb-3">
			  <label for="name" class="form-label">Name</label>
			  <input type="text" class="form-control @error('name') is-invalid @enderror" id="names" name="name" value="{{ old('name') ?? $category->name }}" >
			  @error('name')
			      <span class="invalid-feedback" role="alert">
			          <strong>{{ $message }}</strong>
			      </span>
			  @enderror
			</div>
			<div class="mb-3">
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</form>
	</div>
</div>
@endsection