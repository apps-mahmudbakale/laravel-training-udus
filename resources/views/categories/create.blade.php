@extends('layouts.app')
@section('title', 'Create Categories')
@section('content')
<div class="card">
	<div class="card-body">
		<form action="{{ route('categories.store') }}" method="POST">
			@csrf
			<div class="mb-3">
			  <label for="name" class="form-label">Name</label>
			  <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" >
			  @error('name')
			      <span class="invalid-feedback" role="alert">
			          <strong>{{ $message }}</strong>
			      </span>
			  @enderror
			</div>
			<div class="mb-3">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
		</form>
	</div>
</div>
@endsection