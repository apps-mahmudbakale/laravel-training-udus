@extends('layouts.app')
@section('title', 'Categories')
@section('content')
<div class="card">
	<div class="card-title">Categories</div>
	<a href="{{ route('categories.create') }}" class="btn btn-primary">Create</a>
	<div class="card-body">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($categories as $key => $category)
				   <tr>
				   	<td>{{ ++$key }}</td>
				   	<td>{{ $category->name }}</td>
				   	<td>{{ $category->created_at->toDayDateTimeString() }}</td>
				   	<td>
				   		<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning btn-sm">Edit</a>
				   		<form action="{{ route('categories.destroy', $category->id) }}" method="POST" onClick="return confirm('Are you sure?')">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
				   	</td>
				   </tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection