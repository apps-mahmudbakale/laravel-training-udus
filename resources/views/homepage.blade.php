@extends('layouts.app')
@section('title', 'Home Page')
@section('content')
@if(Session::has('mail'))
      <div class="alert alert-success">{{Session::get('mail')}}</div>
    @endif
<div class="card bg-secondary text-white">
  <div class="card-body">

      <h1 class="display-4">{{ config('app.name') }}</h1>
      <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
  </div>
</div>
@endsection