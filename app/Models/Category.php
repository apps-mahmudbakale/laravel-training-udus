<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    // Fields that are mass assignable 
    protected $fillable = [
        'name'
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
