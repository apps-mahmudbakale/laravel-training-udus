<?php

namespace App\Http\Controllers;

use App\Mail\HomeMail;
use Illuminate\Http\Request;
use Mail;
class HomepageController extends Controller
{
    public function index()
    {
        // Mail::raw('Hello Mahmud', function($message)
        // {
        //     $message->from('test@example.com', 'Test Mail');
         
        //     $message->to('foo@example.com');
        //     $message->subject('Welcome');
        // });

        Mail::to('bakale.mahmud@gmail.com')->send(new HomeMail());
         session()->flash('mail', 'Mail Sent Successfully');
        return view('homepage');
    }
}
