<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleFormRequest;
use App\Mail\TestMail;
use App\Models\Article;
use App\Models\Category;
use App\Notifications\ArticleCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Mail;
class ArticlesController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Session::put('name', 'Mahmud Bakale');
        $articles = DB::table('articles')
        ->select('id', 'title', 'excerpt', 'created_at')->get();
        // $articles = Article::get(['id', 'title', 'excerpt', 'created_at']);
        return view('articles.index', [
            'articles' => $articles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('articles.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleFormRequest $request)
    {
        DB::transaction(function() use ($request) {
        // $article = new Article();
        // $article->title = $request->title;
        // $article->excerpt = $request->excerpt;
        // $article->content = $request->content;
        // $article->image = $request->image->store('images');
        // $article->save();


        // DB::table('articles')
        // ->insert([
        //     'title' => $request->title,
        //     'excerpt' => $request->excerpt,
        //     'content' => $request->content,
        //     'category_id' => $request->category_id,
        //     'image' => $request->image->storeAs('public/images', $request->image->getClientOriginalName())
        // ]);
        $article = Article::create([
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'content' => $request->content,
            'category_id' => $request->category_id
            'image' => $request->image->storeAs('public/images', $request->image->getClientOriginalName())
        ]);
        auth()->user()->notify(new ArticleCreated($article));

        // $article = Article::create($request->except('image'));

        // $article = Article::create($request->except('image'));


        // $article->image = $request->image->store('images');
        // $article->save();
        // Mail::to('bakale.mahmud@gmail.com')->send(new TestMail());
        });

            
            session()->flash('success', 'Article Created Successfully');
            return redirect()->route('articles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $article = DB::table('articles')
        // ->where('id', $id)->first();

        $article = Article::findOrFail($id);

        return view('articles.show', [
            'article' => $article
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $article = DB::table('articles')
        ->where('id', $id)->first();
        // $article = Article::findOrFail($id);

        return view('articles.edit', [
            'article' => $article,
            'categories' => Category::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleFormRequest $request, $id)
    {
        // $article = Article::find($id);
        // $article->title = $request->title;
        // $article->excerpt = $request->excerpt;
        // $article->content = $request->content;
        
        if($request->hasFile('image')) {
            $article->image = $request->image->store('images');
        }
        
        // $article->save();

        // $article->update([
        //     'title' => $request->title,
        //     'excerpt' => $request->excerpt,
        //     'content' => $request->content,
        //     // 'image' => $request->image->store('images')
        // ]);

        // $article = $article->update($request->except('image'));


        DB::table('articles')
        ->where('id', $id)
        ->update([ 
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'content' => $request->content,
            'category_id' => $request->category_id,
            // 'image' => $request->image->storeAs('public/images', $request->image->getClientOriginalName())
        ]);

        // $article->update($request->except('image'));


        // $article->image = $request
        //     ->image
        //     ->storeAs('public/images', $request->image->getClientOriginalName());
        
        // $article->save();

        return redirect()->route('articles.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $article = Article::findOrFail($id);

        // $article->delete();

        DB::table('articles')
        ->where('id', $id)->delete();

        return redirect()->route('articles.index');
    }
}
