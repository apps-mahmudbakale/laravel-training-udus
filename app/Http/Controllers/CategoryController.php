<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryFormRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return view('categories.index', [
            'categories' => Category::all()
        ]);
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(CategoryFormRequest $request)
    {
        Category::create($request->all());

        return redirect()->route('categories.index');
    }

    public function show($id)
    {
        return view('categories.show', [
            'category' => Category::findOrFail($id)
        ]);
    }

    public function edit(Category $category)
    {
        /**
         * This is another implementation 
        */
        return view('categories.edit', [
            'category' => $category
        ]);
    }

    public function update(CategoryFormRequest $request, $id)
    {
        $category = Category::findOrFail($id);

        $category->update($request->all());

        return redirect()->route('categories.index', $category->id);
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('categories.index');
    }

}
