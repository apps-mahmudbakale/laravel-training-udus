<?php

namespace App\Http\Requests;

use App\Rules\Uppercase;
use Illuminate\Foundation\Http\FormRequest;

class ArticleFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
           'title' => 'required',
           'excerpt' => 'required',
           'content' => 'required',
           'image' => ['required', 'mimes:jpg'],
           'category_id' => 'required'
        ];

        if (request()->method() === 'PATCH') {
            unset($data['image'][0]);
        }

        return $data;
    }

    public function messages()
    {
        return [
            // 'title.required' => 'This :attribute is a must',
        ];
    }

}
