<?php

use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class, 'index'])->name('homepage');

Route::middleware(['auth'])->group(function() {

/***
 *  Articles Routes
 */
// Route::get('articles', [ArticlesController::class, 'index'])->name('articles.index');
// Route::get('articles/create', [ArticlesController::class, 'create'])->name('articles.create');
// Route::post('articles/store', [ArticlesController::class, 'store'])->name('articles.store');
// Route::get('/articles/show/{id}', [ArticlesController::class, 'show'])->(articles)

    Route::group([
        'prefix' => 'articles',
        'as' => 'articles.',
        'controller' => ArticlesController::class,
        // 'middleware' => ['auth']
    ], function() {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
        Route::get('/show/{id}', 'show')->name('show');
        Route::get('/{id}/edit', 'edit')->name('edit');
        Route::patch('/{id}/update', 'update')->name('update');
        Route::delete('/{id}/delete', 'destroy')->name('delete');
    });

    /***
     *  Authors Routes
     */
    Route::get('authors', [AuthorsController::class, 'index'])->name('authors.index');

    /***
     *  Categories Routes
     */
    // Route::get('categories', [CategoryController::class, 'index'])->name('categories.index');

    Route::resource('categories', CategoryController::class)->except('show');

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

});
Auth::routes();